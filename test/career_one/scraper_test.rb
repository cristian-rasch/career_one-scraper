require File.expand_path("test/test_helper")
require File.expand_path("lib/career_one/scraper")

module CareerOne
  class ScraperTest < Minitest::Test
    def setup
      @career_one = Scraper.new
      @keywords = "web developer"
    end

    def test_category_filtering
      jobs = @career_one.job_search(@keywords, limit: 1, category: "IT")
      assert_equal 1, jobs.size
    end

    def test_getting_three_jobs
      jobs = @career_one.job_search(@keywords, limit: 3)
      assert_equal 3, jobs.size
    end

    def test_passing_in_a_callback_to_job_search
      job = nil
      @career_one.job_search(@keywords, limit: 1) do |j|
        job = j
      end
      refute_nil job
    end
  end
end
