# CareerOne::Scraper

careerone.com.au job scraper

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'career-one-scraper', git: 'git@bitbucket.org:cristian-rasch/career-one-scraper.git', require: 'career_one/scraper'
```

And then execute:

    $ bundle

## Usage

```ruby
require "career_one/scraper"
require "pp"

career_one = CareerOne::Scraper.new
# Options include:
#   - category - restrict the search to the specified job category
#                as listed by the CareerOne::Scraper.categories method.
#   - limit - how many jobs to retrieve (defaults to 20, max. 20)
#   - proxies - a list of http proxies to tunnel web traffic through, e.g. %w(http://1.2.3.4:8888/ http://5.6.7.8:8888/) -
#               defaults to reading them from the CAREER_ONE_PROXIES env var or nil if missing
#   - logger - a ::Logger instance to log error messages to

# List job Categories
career_one.categories

# Job search
jobs = career_one.job_search("web developer", category: "IT", limit: 1)
pp jobs.first
# #<struct CareerOne::Scraper::Job
#  title=".Net Developer - Contract",
#  id="4d2b3bc76859ddc2",
#  employer="Zinc Recruitment",
#  location="Brisbane",
#  created_on=2015-02-26 15:01:52 UTC,
#  short_description=
#   "<p>Overseeing <b>database</b> design and development. Assisting Digital Designers and User Interface <b>Developers</b> in planning and development of <b>web</b> applications and... <a href=\"http://au.indeed.com/viewjob?jk=4d2b3bc76859ddc2&amp;qd=2wHKSQTrhrEAhc_JfcRkGuVj7aDQV-p6S8tir9sGZy1Po5gU6NajbUTcNG6A3s1lHOpWAKwwGkKe5xdnLTJ-SKDByxix8hSbtosCZv9ejxZ65-7Cv-qjIRzuLFpGaoiBUY1UWchITDGggm-iF-QVP8IzbPATHwLJVn_ibbL3Cpv9mTOm3zaosPR5_snf4DByem7ZFA-k-g-tOS6JWWKvFFyHPHDLfb4FsPjrnEEvdorqAhSzImzghGm1GoZ7nsAp&amp;indpubnum=9787949612898538&amp;atk=19f8bak8k1c014n8\" rel=\"nofollow\" onmousedown=\"indeed_clk(this, '8624');\" target=\"_blank\">More details</a></p>",
#  type="Contract/Temp",
#  description_html=
#   "A great opportunity to join a global resources company in a 3 + month contract. Work within a large established IT team on local and international projects.\n<br>\n<br>\n<b>Role Responsibilities:\n</b><br>\n<ul>\n<li>Overseeing database design and development\n</li>\n<li>Developing and maintaining web based applications and Windows applications\n</li>\n<li>Development using Microsoft.NET, SQL Server, server-side techniques and client-side technology\n</li>\n<li>Assisting in scoping of projects and investigating new technologies\n</li>\n<li>Ensuring all code is correctly stored and maintained at all times in source control\n</li>\n<li>Conducting QA audits and proven ability to promote a culture of high quality development\n</li>\n<li>Assisting Digital Designers and User Interface Developers in planning and development of web applications and websites\n</li>\n<li>Managing project milestones to meet deadlines and follow scope documentation provided by Solution Architect\n</li>\n<li>Liaising with third party hosting to ensure services are provided.\n</li>\n</ul>\n<b>Skills and Experience:\n</b><br>\n<ul>\n<li>A minimum of 3+years .Net Development experience in a commercial environment\n</li>\n<li>Highly experienced in .NET4\n</li>\n<li>A history working on project work within the resources sector will be highly regarded\n</li>\n<li>Be able to demonstrate a high attention to detail and takes responsibility for the quality of their work\n</li>\n<li>Capable of seeking innovative solutions to complex development problems\n</li>\n<li>Remains abreast of latest developments in technology\n</li>\n<li>Demonstrates best practice development patterns\n</li>\n<li>Team working skills and strong communication skills\n</li>\n<li>Proven ability to competently mentor and guide junior .NET Developers appropriately.</li>\n</ul>">
# => #<struct CareerOne::Scraper::Job title=".Net Developer - Contract", id="4d2b3bc76859ddc2", employer="Zinc Recruitment", location="Brisbane", created_on=2015-02-20 12:00:45 -0300, short_description="<p>Overseeing <b>database</b> design and development. Assisting Digital Designers and User Interface <b>Developers</b> in planning and development of <b>web</b> applications and... <a href=\"http://au.indeed.com/viewjob?jk=4d2b3bc76859ddc2&amp;qd=2wHKSQTrhrEAhc_JfcRkGuVj7aDQV-p6S8tir9sGZy1Po5gU6NajbUTcNG6A3s1lHOpWAKwwGkKe5xdnLTJ-SKDByxix8hSbtosCZv9ejxZ65-7Cv-qjIRzuLFpGaoiBUY1UWchITDGggm-iF-QVP8IzbPATHwLJVn_ibbL3Cpv9mTOm3zaosPR5_snf4DByem7ZFA-k-g-tOS6JWWKvFFyHPHDLfb4FsPjrnEEvdorqAhSzImzghGm1GoZ7nsAp&amp;indpubnum=9787949612898538&amp;atk=19f8bak8k1c014n8\" rel=\"nofollow\" onmousedown=\"indeed_clk(this, '8624');\" target=\"_blank\">More details</a></p>", type="Contract/Temp", description_html="A great opportunity to join a global resources company in a 3 + month contract. Work within a large established IT team on local and international projects.\n<br>\n<br>\n<b>Role Responsibilities:\n</b><br>\n<ul>\n<li>Overseeing database design and development\n</li>\n<li>Developing and maintaining web based applications and Windows applications\n</li>\n<li>Development using Microsoft.NET, SQL Server, server-side techniques and client-side technology\n</li>\n<li>Assisting in scoping of projects and investigating new technologies\n</li>\n<li>Ensuring all code is correctly stored and maintained at all times in source control\n</li>\n<li>Conducting QA audits and proven ability to promote a culture of high quality development\n</li>\n<li>Assisting Digital Designers and User Interface Developers in planning and development of web applications and websites\n</li>\n<li>Managing project milestones to meet deadlines and follow scope documentation provided by Solution Architect\n</li>\n<li>Liaising with third party hosting to ensure services are provided.\n</li>\n</ul>\n<b>Skills and Experience:\n</b><br>\n<ul>\n<li>A minimum of 3+years .Net Development experience in a commercial environment\n</li>\n<li>Highly experienced in .NET4\n</li>\n<li>A history working on project work within the resources sector will be highly regarded\n</li>\n<li>Be able to demonstrate a high attention to detail and takes responsibility for the quality of their work\n</li>\n<li>Capable of seeking innovative solutions to complex development problems\n</li>\n<li>Remains abreast of latest developments in technology\n</li>\n<li>Demonstrates best practice development patterns\n</li>\n<li>Team working skills and strong communication skills\n</li>\n<li>Proven ability to competently mentor and guide junior .NET Developers appropriately.</li>\n</ul>">
```
