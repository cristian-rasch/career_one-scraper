require_relative "scraper/version"
require "dotenv"
Dotenv.load
require "open-uri"
require "nokogiri"
require "chronic"
require "logger"

module CareerOne
  class Scraper
    BASE_URL = "http://www.careerone.com.au/".freeze
    SEARCH_PATH = "/job-search/australia".freeze
    DEFAULT_LIMIT = 20
    USER_AGENT = "Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0".freeze

    class Job < Struct.new(:title, :id, :employer, :location, :created_on,
                           :short_description, :type, :description_html, :url)
    end

    # Options include:
    #   - category - restrict the search to the specified job category
    #                as listed by the CareerOne::Scraper.categories method.
    #   - limit - how many jobs to retrieve (defaults to 20, max. 20)
    #   - proxies - a list of http proxies to tunnel web traffic through, e.g. %w(http://1.2.3.4:8888/ http://5.6.7.8:8888/) -
    #               defaults to reading them from the CAREER_ONE_PROXIES env var or nil if missing
    #   - logger - a ::Logger instance to log error messages to
    def initialize(options = {})
      @options = options
    end

    # Options include:
    #   - category - restrict the search to the specified job category
    #                as listed by the CareerOne::Scraper.categories method.
    #   - limit - how many jobs to retrieve (defaults to 20, max. 20)
    #   - logger - a ::Logger instance to log error messages to
    #   - callback - an optional block to be called with each job found ASAP
    def job_search(keywords, options = {})
      configure_logger(options[:logger])

      limit = (options[:limit] || @options[:limit] || DEFAULT_LIMIT)
      limit = [limit, DEFAULT_LIMIT].min

      job_search_url = URI.join(BASE_URL, SEARCH_PATH).to_s
      category = options[:category] || @options[:category]
      category = (categories & [category]).first
      if category
        category_path = category.scan(/\w+/).map(&:downcase).join("-")
        job_search_url << "/#{category_path}"
      end

      kws = keywords.gsub(/\s+/, "-")
      job_search_url << "/#{kws}"

      job_search_url << "/casual?j_c=71&j_l=73&j_a=3560&where=australia"
      job_search_url << "&category_name=#{CGI.escape(category)}" if category
      job_search_url << "&jt[]=20"

      jobs = []
      io = open_through_proxy(job_search_url)
      return jobs unless io

      jobs_page = Nokogiri::HTML(io)
      jobs_page.css(".job").each do |job_node|
        title_link = job_node.at_css(".job_title a")
        title = title_link.text
        job_url = title_link["href"]
        id = job_url[/\?jk=([^&]+)/, 1]
        employer_node = job_node.at_css(".job_company")
        employer = employer_node.text if employer_node
        location = job_node.at_css(".job_location").text
        created_on = job_node.at_css(".job_date").text
        created_on = $1 if created_on =~ /(Today)\z/
        created_on = Chronic.parse(created_on)
        created_on ||= Date.today.to_datetime.to_time
        created_on = created_on.utc
        short_description_html = job_node.at_css(".bottom .left").inner_html.strip

        if io = open_through_proxy(job_url)
          job_page = Nokogiri::HTML(io)

          if description_node = job_page.at_css("#job_summary")
            description_html = description_node.inner_html.rstrip
          end

          if type_node = job_page.at_css("#job_header")
            type = type_node.text.split(/\s+/).last
          end
        end

        job = Job.new(title, id, employer, location, created_on, short_description_html,
                      type, description_html, job_url)
        yield(job) if block_given?
        jobs << job

        break if jobs.size == limit
      end

      jobs
    end

    def categories
      @categories ||= begin
                        if io = open_through_proxy(BASE_URL)
                          Nokogiri::HTML(io).
                            at_css('select[name="jcat"]').
                            css("option").
                            map(&:text)[1..-1]
                        else
                          []
                        end
                      end
    end

    private

    def configure_logger(logger = nil)
      @logger = logger || @options[:logger] || Logger.new(STDERR)
    end

    def open_through_proxy(url)
      begin
        open(url, "User-Agent" => USER_AGENT, "proxy" => next_proxy)
      rescue => err
        @logger.error "#{err.message} opening '#{url}'"
        nil
      end
    end

    def next_proxy
      proxies.shift.tap { |proxy| proxies << proxy }
    end

    def proxies
      @proxies ||= (@options[:proxies] || ENV.fetch("CAREER_ONE_PROXIES").split(",")).shuffle
    end
  end
end
